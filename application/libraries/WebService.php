<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    class WebService{
        public function run($url, $header, $param){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            //Header = array('Content-Type: application/json' , "Authorization: Bearer 080042cad6356ad5dc0a720c18b53b8e53d4c274")
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$param);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            
            curl_close ($ch);

            return $result;
        }
    }

?>