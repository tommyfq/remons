<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perangkat extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('UserModel');
    $this->load->model('DeviceModel');
  }

	public function index()
	{  
    $nama = $this->session->userdata('nama_depan');

    if ($this->session->has_userdata('logged_in')) {
      $data = array(  
        'nama' => $nama,
        'active' => 'Perangkat',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Perangkat',
        'title'=>'Perangkat'
      );

      $this->load->view('master_page', $data);
    }else{
      $this->load->view('login');
    }
  }
        
  public function edit(){
    $userid = $this->session->userdata('id');
    $input_device = $this->input->get('device');
    $nama = $this->session->userdata('nama_depan');
    $device = $this->DeviceModel->getRowDevice($input_device, $userid);

    if ($this->session->has_userdata('logged_in')) {
      $data = array(  
        'nama' => $nama,
        'active' => 'Perangkat',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Tambah_Perangkat',
        'title'=>'Perangkat',
        'perangkat'=>$device,
        'url'=>'Perangkat/update'
      );

      $this->load->view('master_page', $data);

    }else{
      $this->load->view('login');
    }
  }

  public function editp(){ 
    $device_code = $this->input->post('perangkat');
    $path = 'public/'.$device_code;

    $files = array();
    $res = array();

    $config['upload_path'] = $path;
    $config['allowed_types'] = 'pdf|doc*|jpg';
    $config['max_size'] = '10000';

    $this->load->library('upload',$config);
    
    if(!is_dir($path)) mkdir($path,0777,true);

    foreach($_FILES as $key => $value){
      $files[$key] = null;
      if($value['name'] != ""){
        if(!$this->upload->do_upload($key)){
          array_push($res,array('error' => $this->upload->display_errors()));
        }else{
          $files[$key] = $this->upload->data('file_name');
        }
      }
    }

    $device_register = array(
      'device_code'   => $device_code,
      'site_name'     => $this->input->post('site_name'),
      'latitude'      => $this->input->post('latitude'),
      'longitude'     => $this->input->post('longitude'),
      'date_updated'  => date("Y-m-d H:i:s")
    );

    if ($this->input->post('capacity') != "") {
      $device_register['capacity'] = $this->input->post('capacity');
    }
    if ($this->input->post('no_mobile_site') != "") {
      $device_register['no_mobile_site'] = $this->input->post('no_mobile_site');
    }
    if ($this->input->post('contact_operator') != "") {
      $device_register['contact_operator'] = $this->input->post('contact_operator');
    }
    if ($this->input->post('phone_number') != "") {
      $device_register['phone_number'] = $this->input->post('phone_number');
    }
    if ($this->input->post('install_contractor') != "") {
      $device_register['install_contractor'] = $this->input->post('install_contractor');
    }
    if ($this->input->post('install_year') != "") {
      $device_register['install_year'] = $this->input->post('install_year');
    }
    if ($this->input->post('manual_book') != "") {
      $device_register['manual_book'] = $this->input->post('manual_book');
    }
    if ($this->input->post('wiring_diagram') != "") {
      $device_register['wiring_diagram'] = $this->input->post('wiring_diagram');
    }
    if ($this->input->post('survey_document') != "") {
      $device_register['survey_document'] = $this->input->post('survey_document');
    }
    if ($this->input->post('address') != "") {
      $device_register['address'] = $this->input->post('address');
    }
    if ($this->input->post('area') != "") {
      $device_register['area'] = $this->input->post('area');
    }
    if ($this->input->post('province') != "") {
      $device_register['province'] = $this->input->post('province');
    }
    if ($this->input->post('time_zone') != "") {
      $device_register['time_zone'] = $this->input->post('time_zone');
    }

    try{
      $this->DeviceModel->updateDevice($device_register);
      $flash_msg = "Edit Device Success";
    }catch(Exception $e){
      $flash_msg = "Sedang terjadi masalah jaringan internal. Mohon untuk mencoba kembali";
      $flash_name = "new_device_error";
    }

    $this->session->set_flashdata($flash_msg, $flash_msg);
    redirect('/Dashboard');
  }

}