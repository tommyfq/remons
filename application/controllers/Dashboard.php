<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('UserModel');
	// $this->load->model('M_home');
	$this->load->model('DeviceModel');
  }

	public function index()
	{  
		$user = $this->session->userdata();
		if ($this->session->has_userdata('logged_in')) {
			// $variabel_pohon = $this->M_home->getVariabelPohon();
			// $variabel_co2 = $this->M_home->getVariabelCO2();

			$jml_plts = $this->DeviceModel->getUserDevice($user['id']);

			$total_kapasitas = 0;

			/*highlight_string("<?php\n\$data =\n" . var_export($jml_plts, true) . ";\n?>");
			die;*/

			// $last_processed_log = $this->M_home->getLastProcessed();
			// $summaryInverter = $this->M_home->getSummaryInverter();
			// $summaryCharger = $this->M_home->getSummaryCharger();

			// $warningAlertInverter = $this->M_home->getSummaryInverter();
			// $warningAlertCharger = $this->M_home->getSummaryCharger();

			// $data_plts = $this->M_home->getPlts();
			//var_dump($data_plts);
			$data = array(  
				'nama' => $user['nama_depan'],
				'active' => 'Dashboard',
				'sidebar'=>'nav_Sidebar',
				'header'=>'nav_Header',
				'content'=>'content_Dashboard',
				'title'=>'Dashboard',
				// 'variabel_pohon' => $variabel_pohon,
				// 'variabel_co2' => $variabel_co2,
				'jml_plts' => count($jml_plts),
				'total_kapasitas' => $total_kapasitas,
				// 'last_processed_log' => $last_processed_log,
				// 'summaryInverter' => $summaryInverter,
				// 'summaryCharger' => $summaryCharger,
				// 'data_plts' => $data_plts,
				// 'warningAlertInverter' => $warningAlertInverter,
				// 'warningAlertCharger' => $warningAlertCharger,
				'footer' => 'dashboard_script'
			);

			if($jml_plts != NULL){
				foreach($jml_plts as $row){
					$total_kapasitas += $row['capacity'];
				}
				$data['data_device'] = $jml_plts;
			}

			// echo "<pre>"; print_r($data); die;

			$this->load->view('master_page', $data);
		}else{
			$this->load->view('login');
		}
	}
}