<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambah_Perangkat extends CI_Controller {

  private $nama;
  private $username;

  public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->helper('url', 'form');
    $this->load->model('UserModel');
    $this->load->model('DeviceModel');
  }

  public function index(){

    $this->nama = $this->session->userdata('nama_depan');
		if ($this->session->has_userdata('logged_in')) {

    	$data = array(
        'nama' => $this->nama,  
        'active' => 'Monitoring',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Tambah_Perangkat',
        'title'=>'Tambah Perangkat'
      );
    
        $this->load->view('master_page', $data);
    }else{
      $this->load->view('login');
    }
  }
  
  public function addDevice(){

    $this->nama = $this->session->userdata('nama_depan');
    $device_code = $this->input->post('device_code');
    $path = 'public/'.$device_code;

    $files = array();
    $res = array();

    $config['upload_path'] = $path;
    $config['allowed_types'] = 'pdf|doc*|jpg';
    $config['max_size'] = '10000';

    $this->load->library('upload',$config);
    
    if(!is_dir($path)) mkdir($path,0777,true);

    foreach($_FILES as $key => $value){
      $files[$key] = null;
      if($value['name'] != ""){
        if(!$this->upload->do_upload($key)){
          array_push($res,array('error' => $this->upload->display_errors()));
        }else{
          $files[$key] = $this->upload->data('file_name');
        }
      }
    }

    //var_dump($this->input->post('time_zone'));
   // die();

    //Get input post data
    $device_register = array(
      'device_code' => $device_code,
      'license_code' => $this->input->post('license_code'),
      'site_name' => $this->input->post('site_name'),
      'address' => $this->input->post('address'),
      'area' => $this->input->post('area'),
      'province' => $this->input->post('province'),
      'time_zone' => $this->input->post('time_zone'),
      'latitude' => $this->input->post('latitude'),
      'longitude' => $this->input->post('longitude'),
      'capacity' => $this->input->post('capacity'),
      'status' => $this->input->post('status'),
      'no_mobile_site' => $this->input->post('no_mobile_site'),
      'contact_operator' => $this->input->post('contact_operator'),
      'phone_number' => $this->input->post('phone_number'),
      'install_contractor' => $this->input->post('install_contractor'),
      'install_year' => $this->input->post('install_year'),
      'manual_book' => $files['manual_book'],
      'wiring_diagram' => $files['wiring_diagram'],
      'survey_document' => $files['survey_document']
    );

    $data = array(
      'nama' => $this->nama,  
      'active' => 'Monitoring',
      'sidebar'=>'nav_Sidebar',
      'header'=>'nav_Header',
      'content'=>'content_Monitoring',
      'title'=>'Monitoring',
      'footer' => 'chart_script'
    );

    try{
      $this->DeviceModel->insertNewDevice($device_register);
    }catch(Exception $e){
      $flash_msg = "Sedang terjadi masalah jaringan internal. Mohon untuk mencoba kembali";
      $flash_name = "new_device_error";
    }

    $this->session->set_flashdata($flash_msg, $flash_msg);
    redirect('/Perangkat');
  }

}

