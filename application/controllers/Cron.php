<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

  private $user;

	public function __construct() {

    parent::__construct();

    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('ConfigModel');
    
    //Windows
    $this->load->library('webservice');
    //Linux
    //$this->load->library('WebService');

    //$this->load->model('ConfigModel');
    
    $this->load->model('DeviceModel');

    $this->load->model('DeviceMonitoringModel');

  }

    private function checkAuth($auth){

    }
    public function index(){  
        return "YOU SHOULD NOT PASS";
    }

    public function dataDailyUpdate(){
      $webservice = new WebService();
      //$auth = $this->input->get_request_header('Authorization');
      //$value = $this->input->raw_input_stream;
      $config = $this->ConfigModel->getConfig('API','Daily Update');
      $token = '';
      $url = '';
      foreach($config as $line){
        if($line['attribute'] == 'api_token') $token = $line['value'];
        if($line['attribute'] == 'url') $url = $line['value'];
      }

      $header = array(
          "Content-Type: application/json", 
          "Authorization: Bearer ".$token
        );

      $devices = $this->DeviceModel->getActiveDevice();

      $param = array();  
      foreach($devices as $device){
        array_push($param,$device['device_code']);
      }

      $param = json_encode(array('kode' => $param));

      $data_monitoring_raw = $webservice->run($url,$header,$param);

      $data_monitoring_raw = json_decode($data_monitoring_raw);

      foreach($data_monitoring_raw as $row){
        $row_array = (array) $row;
        foreach($row_array as $line){
          
          if($line->akumulasi != null){
            $line->akumulasi = (array) $line->akumulasi;
            for($i = 0; $i < count($line->per_menit);$i++){
              $line->per_menit[$i] = (array)$line->per_menit[$i];
            }

            $result = $this->DeviceMonitoringModel->insertDeviceData($line->akumulasi['device_code'], $line->per_menit,$line->akumulasi);
          }
        }
      }
  }

  public function testData(){
      var_dump(uniqid());
      die;
    $var = array(
      array(
        "device" => "REMONS190006",
        "date" => "2019-11-24",
        "time" => "00:01:10",
        "data" => array(
          "data_l1" => 0.1,
          "data_l2" => 0.2,
          "data_l3" => 0.3,
          "tegangan_l1" => 0.1,
          "tegangan_l2" => 0.1,
          "tegangan_l3" => 0.1,
          "arus_l1" => 0.1,
          "arus_l1" => 0.1,
          "frekuensi" => 10,
          "energi_total" => 0.5,
          "daya_total" => 0.1
        )
      )
    );
    var_dump(json_encode($var));
    die;
    return json_encode($var);
  }

}

