<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganti_Password extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('UserModel');
  }

	public function index()
	{
    $nama = $this->session->userdata('nama_depan');

		if ($this->session->has_userdata('logged_in')) {
      $data = array(
        'nama' => $nama,  
        'active' => 'Ganti_Password',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Ganti_Password',
        'title'=>'Ganti Password'
      );
      $this->load->view('master_page', $data);

    }else{
      $this->load->view('login');
    }
  }
  
  public function edit()
  {
    $oldpass = $this->input->post('oldpass');
    $newpass = $this->input->post('newpass');
    $username = $this->session->userdata('username');
    $date = date("Y-m-d H:i:s");

    $pass = $this->UserModel->getUserCredential($username);

    if (password_verify($oldpass, $pass)) {
      $options = [
        'cost' => 12,
      ];

      $password = password_hash($newpass, PASSWORD_BCRYPT, $options);

      $this->db->set('password', $password);
      $this->db->set('updatedt', $date);
      $this->db->where('username', $username);
      $this->db->update('tbl_user');

      $this->session->set_flashdata("ganti_password_message","Ganti Password Berhasil !");
      redirect('Ganti_Password','refresh');
    }else{
      $this->session->set_flashdata("ganti_password_message","Ganti Password Gagal !");
      redirect('Ganti_Password','refresh');
    }
  }

}