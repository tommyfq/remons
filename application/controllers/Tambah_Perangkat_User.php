<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambah_Perangkat_User extends CI_Controller {
  private $nama;
  public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('UserModel');
    $this->load->model('DeviceModel');
    $this->load->model('ConfigModel');
    $this->load->model('DeviceMonitoringModel');
    //Windows
    // $this->load->library('webservice');
    //Linux
    $this->load->library('WebService');

  }

	public function index()
	{
    //print_r(sha1('I8EY7H1RIKK300OA4L9RGWU285WWZSOG'));
    //die;
    $this->nama = $this->session->userdata('nama_depan');

		if ($this->session->has_userdata('logged_in')) {
    	$data = array(
        'nama' => $this->nama,  
        'active' => 'Monitoring',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Tambah_Perangkat_User',
        'title'=>'Tambah Perangkat'
      );
      
      $this->load->view('master_page', $data);
    }else{
      $this->load->view('login');
    }
  }

  private function firstFetchData($device_code){

    $param = array("kode" => $device_code);
    
    $config = $this->ConfigModel->getConfig('API','First Fetch');
    $token = '';
    $url = '';
    foreach($config as $line){
      if($line['attribute'] == 'api_token') $token = $line['value'];
      if($line['attribute'] == 'url') $url = $line['value'];
    }
    $header = array(
      "Content-Type: application/json" , 
      "Authorization: Bearer ".$token
    );
    
    $param = json_encode($param);
    
    $webservice = new WebService();
    $response = $webservice->run($url,$header,$param);

    $data_monitoring = (array) json_decode($response);

    if ($data_monitoring['code'] == '200') {

      for($i = 0; $i < count($data_monitoring['per_menit']); $i++){
        $data_monitoring['per_menit'][$i] = (array) $data_monitoring['per_menit'][$i];
      }
      $data_monitoring['akumulasi'] = (array) $data_monitoring['akumulasi'];
      
      $result = $this->DeviceMonitoringModel->insertDeviceData($device_code, $data_monitoring['per_menit'],$data_monitoring['akumulasi']);
    }
  }

  public function addDevice(){
    //Get input post data
    $license_code = $this->input->post('license_code');
    $device_code = $this->input->post('device_code');
    $user = $this->session->userdata();
    //var_dump($device_code);
    //die;
    $data = array(
      'nama' => $this->nama,  
      'active' => 'Monitoring',
      'sidebar'=>'nav_Sidebar',
      'header'=>'nav_Header',
      'content'=>'content_Monitoring',
      'title'=>'Monitoring',
      'footer' => 'chart_script'
    );

    $flash_msg = "Kode lisensi atau kode perangkat tidak valid";
    $flash_name = "new_device_error";
    $page = '/Tambah_Perangkat_User';

    try{
      //check assigned device
      $res = $this->DeviceModel->checkAssignedDevice($license_code, $device_code, $user['id']);
      
      if($res == true){
        //set param for insert
        $param = array(
          'license_code' => $license_code,
          'user_id'      => $user['id'],
          'date_updated' => date('Y-m-d H:i:s'),
          'date_added'   => date('Y-m-d H:i:s'),
          'status'       => '1',
        );
          //assign device to user
          $this->DeviceModel->assignDevice($param);

          //Get First Fetch Data
          $data = $this->DeviceMonitoringModel->getLastData($device_code);
          //If device already fetch
          if($data == null) $this->firstFetchData($device_code);
          

          $flash_msg = "Nomor lisensi ".$license_code." telah berhasil ditambahkan.";
          $flash_name = "new_device_succes";
          $page = '/Monitoring';
      }
    }catch(Exception $e){
      $flash_msg = "Sedang terjadi masalah jaringan internal. Mohon untuk mencoba kembali";
      $flash_name = "new_device_error";
    }

    $this->session->set_flashdata($flash_name, $flash_msg);
    redirect($page);
  }
  
}