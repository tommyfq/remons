<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('UserModel');
  }
	
	public function index(){
		if ($this->session->has_userdata('logged_in')) {
      redirect('/Dashboard', 'refresh');
    }else{
      $this->db->trans_begin();

      $password = $this->input->post('passwordReg');
      $options = [
        'cost' => 12,
      ];

      $user_register = array(
        'username' => $this->input->post('usernameReg'),
        'password' => password_hash($password, PASSWORD_BCRYPT, $options),
        'nama_depan' => $this->input->post('nama_depan'),
        'nama_belakang' => $this->input->post('nama_belakang'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'tanggal_lahir' => $this->input->post('tgl_lahir'),
        'email' => $this->input->post('email'),
        'telepon' => $this->input->post('phone'),
        'alamat' => $this->input->post('alamat'),
        'createdt'  => date("Y-m-d H:i:s"),
        'id_role' => '1'
      );

      $username_check = $this->UserModel->username_check($user_register['username']);

      $email_check = $this->UserModel->email_check($user_register['email']);
      
      if ($username_check == false) {
        if($email_check == true){
          $user_id = $this->UserModel->Register($user_register);
          $this->session->set_flashdata('success_msg', 'Registered successfully.');
          
          $add_detail = $this->UserModel->AddUserDetail($user_id);
          $userDetail = $this->UserModel->get_user($user_register['username']);
          
          if ($add_detail == true) {
            $user = array(
              'logged_in' => true,
              'nama'      => $user_register['nama_depan'].' '. $user_register['nama_belakang'],
              'email'     => $user_register['email']
            );

            $this->sendEmail($user_id, $user_register['username'], $user_register['email'], $userDetail['token'], $user);
            
          }else{
            $this->session->set_flashdata('error_msg', 'Proses Registrasi anda Gagal !');
            redirect('/Login');
          }
          
        }else{
          $this->session->set_flashdata('error_msg', 'Email yang anda masukkan salah atau sudah digunakan oleh user lain. Gunakan Email yang lainnya.');
          redirect('/Login');
        }
      }else{
        $this->session->set_flashdata('error_msg', 'Username yang anda masukkan salah atau sudah digunakan oleh user lain. Gunakan Username yang lainnya.');
        redirect('/Login');
      }
    }
  }

  public function EmailActivation(){

    $userid =  $this->uri->segment(3);
    $token = $this->uri->segment(4);
        
    $user = $this->UserModel->getUserById($userid);

    $tokendt = new DateTime($user->tokendt);
    $diff = $tokendt->diff(new DateTime(date('Y-m-d H:i:s')));
    $minutes = $diff->days * 24 * 60;
    $minutes += $diff->h * 60;
    $minutes += $diff->i;
    
    if ($user != NULL) {
      if ($user->status == 0) {
        if ($minutes <= 10) {
          $this->UserModel->updateStatus($user->userid);
      
          $this->session->sess_destroy();

          $this->load->view('content_User_Aktif');
        }else{
          // $this->getNewToken($user->userid);
          $data['userid'] = $user->userid;
          $this->load->view('content_Token_Invalid', $data);
        }
      }else{
        redirect('/Login');
      }
    }else{
      redirect('/Login');
    }
  }
  
  public function getNewToken(){

    $userid = $this->input->post('user');
    
    if ($userid != NULL) {
      $newToken = $this->UserModel->updateUserDetail($userid);
      $user = $this->UserModel->getUserById($userid);

      $data = array(
        'logged_in' => true,
        'nama'      => $user->nama_depan.' '. $user->nama_belakang,
        'email'     => $user->email
      );

      if ($newToken == true) {
        $this->sendEmail($userid, $user->username, $user->email, $user->token, $data);
      }else{
        redirect('/Login');
      }
    }else{
      redirect('/Login');
    }
    
  }

  function sendEmail($userid, $username, $email, $token, $user){
    //set up email
    $config = array(
      'protocol'  => 'smtp',
      'smtp_host' => 'ssl://mail.remonscloud.com',
      // 'smtp_host' => 'ssl://smtp.mail.remonscloud.com',
      'smtp_port' => 465,
      'smtp_user' => 'admin@remonscloud.com', // change it to yours
      'smtp_pass' => 'RMS!@#123', // change it to yours
      'mailtype'  => 'html',
      'starttls'  => true,
      'charset'   => 'iso-8859-1',
      'wordwrap'  => TRUE,
      'newline'   => "\r\n",
    );

    $message = "
      <html>
      <head>
        <title>Kode Verifikasi</title>
      </head>
      <body>
        <h2>Terima kasih telah melakukan registrasi di website Remons !</h2>
        <br>
        <p>Username: ".$username."</p>
        <p>Silahkan klik link dibawah ini untuk mengaktifkan akun Remons anda. Link ini hanya aktif selama 10 menit. Jika lebih dari 10 menit, link ini akan kadaluarsa dan anda harus meminta link baru dengan menekan tombol dibawah ini.</p>
        <h4><a href='".base_url()."Register/EmailActivation/".$userid."/".$token."'>Activate My Account</a></h4>
      </body>
      </html>
    ";

    // $this->load->library('email', $config);
    $this->load->library('email');
    $this->email->initialize($config);
    // $this->email->set_newline("\r\n");
    $this->email->from($config['smtp_user']);
    $this->email->to($email);
    $this->email->subject('Signup Verification Email');
    $this->email->message($message);

    //sending email
    if($this->email->send()){
      $this->session->set_flashdata('message','Activation code sent to email');

      $this->db->trans_commit();

      $this->session->set_userdata($user);
      redirect('/RegisterNotif', 'refresh');
    }else{
      $this->db->trans_rollback();
      
      $this->session->set_flashdata('message', $this->email->print_debugger());
      $this->session->set_flashdata('error_msg', 'Gagal Kirim Email !');
      redirect('/Login');
    }
  }

  function sendEmail_BAK($userid, $username, $email, $token, $user){
    $mail = new PHPMailer(true);

    $auth = true;

    if ($auth) {
      $mail->IsSMTP(); 
      $mail->SMTPAuth = true; 
      $mail->SMTPSecure = "ssl"; 
      $mail->Host = "smtp.remonscloud.com"; 
      $mail->Port = 465; 
      $mail->Username = "admin@remonscloud.com"; 
      $mail->Password = "RMS!@#123"; 
    }

    $message = "
      <html>
      <head>
        <title>Kode Verifikasi</title>
      </head>
      <body>
        <h2>Terima kasih telah melakukan registrasi di website Remons !</h2>
        <br>
        <p>Username: ".$username."</p>
        <p>Silahkan klik link dibawah ini untuk mengaktifkan akun Remons anda. Link ini hanya aktif selama 10 menit. Jika lebih dari 10 menit, link ini akan kadaluarsa dan anda harus meminta link baru dengan menekan tombol dibawah ini.</p>
        <h4><a href='".base_url()."Register/EmailActivation/".$userid."/".$token."'>Activate My Account</a></h4>
      </body>
      </html>
    ";

    $mail->AddAddress($email);
    $mail->SetFrom("admin@remonscloud.com", "Remons Admin");
    $mail->isHTML(true);
    $mail->Subject = "Signup Verification Email";
    $mail->Body = $message;

    //sending email
    if($this->email->send()){
      $this->session->set_flashdata('message','Activation code sent to email');

      $this->db->trans_commit();

      $this->session->set_userdata($user);
      redirect('/RegisterNotif', 'refresh');
    }else{
      $this->db->trans_rollback();
      
      $this->session->set_flashdata('message', $this->email->print_debugger());
      $this->session->set_flashdata('error_msg', 'Gagal Kirim Email !');
      redirect('/Login');
    }

    try {
      if($mail->Send()){
        $this->session->set_flashdata('message','Activation code sent to email');

        $this->db->trans_commit();

        $this->session->set_userdata($user);
        redirect('/RegisterNotif', 'refresh');
      }
    } catch(Exception $e){
      $this->db->trans_rollback();
      
      $this->session->set_flashdata('message', $this->email->print_debugger());
      $this->session->set_flashdata('error_msg', $mail->ErrorInfo);
      redirect('/Login');
    }
  }

}