<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {
  private $user;
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('UserModel');
    $this->load->model('DeviceModel');
    $this->load->model('DeviceMonitoringModel');
  }

  public function index($device = '', $date = '')
	{  
    $this->user = $this->session->userdata();
    
		if ($this->session->has_userdata('logged_in')) {
      $device_code = $this->input->get('device');
      $date = $this->input->get('date');
      
      $user_devices = $this->DeviceModel->getUserDevice($this->user['id']);
      //var_dump(empty($user_devices));
      //die;
      //Set date now
      if(!empty($user_devices)){
        if($date == null && $device == null){
          $date = date('Y-m-d');
          $device_code = $user_devices[0]['device_code'];
        }
      }

      //Get last data
      $last_data = array();
      $last_data = $this->DeviceMonitoringModel->getLastData($device_code);
      $daya_total = $this->DeviceMonitoringModel->getLastPowerData($device_code);
      //$last_data['daya_total'] = $daya_total;

      //Get Data Energy and Power by date
      $data_daily = $this->DeviceMonitoringModel->getDataDaily($date);

      $times = array();

      $energy_data_log = array();

      $power_data_log = array();

      //Set data for chart monitoring daily
      foreach($data_daily as $line){
        array_push($times,$line['time']);
        array_push($energy_data_log,$line['energi_total']);
        array_push($power_data_log,$line['daya_total']);
      }

      //Get Chosen month and year
      $month_chosen = date('m',strtotime($date));
      $year_chosen = date('Y',strtotime($date));

      // Get day of month 
      $count_days = cal_days_in_month(CAL_GREGORIAN, $month_chosen, $year_chosen);
      
      $days = array();
      
      $energy_data_daily = array();
      $power_data_daily = array();

      //Get Data for 30 days
      $data_monthly = $this->DeviceMonitoringModel->getDataMonthly($month_chosen, $year_chosen, $device_code);

      //Get Previous data before
      $prev_data_monthly = $this->DeviceMonitoringModel->getPreviousDataMonthly($date,$device_code);
      if(empty($prev_data_monthly)) $prev_data_monthly['energi_total'] = 0;
      
      for($i = 1; $i <= $count_days; $i++){
        $flag = 0;
        array_push($days,$i);

        foreach($data_monthly as $data_monthly_line){
          if($flag == 0){
            if((int)$data_monthly_line['day'] == $i){
              array_push($energy_data_daily,$data_monthly_line['energi_total']);
              $prev_data_monthly['energi_total'] = $data_monthly_line['energi_total'];
              $flag = 1;
            }  
          }
        }
        
        if($flag == 0){
          array_push($energy_data_daily,$prev_data_monthly['energi_total']);
        }  
        
      }
      /*highlight_string("<?php\n\$data =\n" . var_export($power_data_daily, true) . ";\n?>");
      die;*/
      //var_dump($energy_data_daily);
      //die;
      $data_yearly = $this->DeviceMonitoringModel->getDataYearly($year_chosen,$device_code);
      /*highlight_string("<?php\n\$data =\n" . var_export($data_yearly, true) . ";\n?>");
      die;*/
      $prev_data_yearly = $this->DeviceMonitoringModel->getPreviousDataYearly($date, $device_code);
      if(empty($prev_data_yearly)) $prev_data_yearly['energi_total'] = 0;

      $months = array();
      $energy_data_monthly = array();
      $power_data_monthly = array();

      for($j = 1; $j <= 12; $j++){
        array_push($months,date("M",strtotime("2019-".$j."-01")));
        $flag = 0;
        foreach($data_yearly as $data_yearly_line){
          if($flag == 0){
            if($j == $data_yearly_line['month']){
              array_push($energy_data_monthly,$data_yearly_line['energi_total']);
              $prev_data_yearly['energi_total'] = $data_yearly_line['energi_total'];
              $flag = 1;
            }
          }
        }
        if($flag == 0){
          array_push($energy_data_monthly,$prev_data_yearly['energi_total']);
        }
        
      }
      /*highlight_string("<?php\n\$data =\n" . var_export($energy_data_monthly, true) . ";\n?>");
      die;*/
    	$data = array( 
        'nama' => $this->user['nama_depan'], 
        'active' => 'Monitoring',
        'sidebar'=>'nav_Sidebar',
        'header'=>'nav_Header',
        'content'=>'content_Monitoring',
        'title'=>'Monitoring',
        'footer' => 'monitoring_script',
        'label_hour' => $times,
        'energy_data_log' => $energy_data_log,
        'power_data_log' => $power_data_log,
        'label_day' => $days,
        'energy_data_daily' => $energy_data_daily,
        'power_data_daily' => $power_data_daily,
        'label_month' => $months,
        'energy_data_monthly' => $energy_data_monthly,
        'power_data_monthly' => $power_data_monthly
      );

      $data['devices'] = $user_devices;
      $data['last_data'] = $last_data;
      $data['date'] = $date;

      $date_now = strtotime($date);
      $last_date = strtotime($last_data['date']);

      $hour_diff = $date_now - $last_date;

      $data['hour_diff'] = $hour_diff;
      
      $this->load->view('master_page', $data);

    }else{
      $this->load->view('login');
    }
  }
  
  public function getData(){
    
  }

}

