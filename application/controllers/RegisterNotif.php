<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterNotif extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->load->model('UserModel');
  }

	public function index(){  
		if ($this->session->has_userdata('logged_in')) {
			$data = array(  
				'title'	=> 'Register',
				'nama'	=> $this->session->userdata('nama'),
				'email'	=> $this->session->userdata('email')
			);

			$this->session->sess_destroy();

			$this->load->view('content_Sukses_Registrasi', $data);
		}else{
			$this->load->view('login');
		}
  }

}