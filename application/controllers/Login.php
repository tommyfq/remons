<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    
    $this->load->model('UserModel');
  }

	public function index()
	{
    $this->session->sess_destroy();
    
		if ($this->session->has_userdata('logged_in')) {
      redirect('/Dashboard', 'refresh');
    }else{
      $this->load->view('login');
    }
	}

	public function doLogin(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');


    $check = false;
    $check = $this->UserModel->username_check($username);

    if($check === true){
      // $options = [
      //   'cost' => 12,
      // ];

      // $hash = password_hash($password, PASSWORD_BCRYPT, $options);
      $pass = $this->UserModel->getUserCredential($username);

      // if($this->UserModel->checkUser($username, $hash)==1){
      if (password_verify($password, $pass)) {

        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
          $secret = '6Lekj8YUAAAAAEPhwAaCUQGc01mcssva0EvktPrj';
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);
          if($responseData->success)
          {
            $succMsg = 'Your contact request have submitted successfully.';
          }
          else
          {
            $errMsg = 'Robot verification failed, please try again.';
          }
        }

        $results = $this->UserModel->get_user($username);

        if ($results != NULL) {
          if ($results['status'] == 0) {
            $user = array(
              'logged_in' => true,
              'nama'      => $results['nama_depan'].' '. $results['nama_belakang'],
              'email'     => $results['email']
            );

            $this->session->set_userdata($user);
            redirect('/RegisterNotif', 'refresh');
          }else{
            $user = array(
              'username' => $results['username'],
              'nama_depan' => $results['nama_depan'],
              'id' => $results['id'],
              'role' => $results['id_role'],
              'logged_in' => true
            );
      
            $this->session->set_userdata($user);
      
            redirect('/Dashboard', 'refresh');
          }
        }else{
          $this->session->set_flashdata("error_msg","Login Gagal, Data user tidak valid!");
          redirect('login','refresh');
        }

      }else{
        $this->session->set_flashdata("error_msg","Password tidak tepat. Silahkan masukkan kembali password anda!");
        redirect('login','refresh');
      }    
    }else{
      $this->session->set_flashdata("error_msg","Username anda tidak terdaftar. Lakukan registrasi untuk masuk ke halaman utama!");
      redirect('login','refresh');
    }       
	}

  public function doLogout(){
    $this->session->sess_destroy();
    redirect('/Home', 'refresh');
  }

}

