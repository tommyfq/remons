<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-comment-o"></i> Perangkat <small style="colour:black;"><?php echo $perangkat['device_code'];?></small> </h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-comment-o"></i> <a href="<?php echo base_url('Monitoring');?>"> Monitoring</a></li>
                    <li>Ubah Perangkat</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><i class="fa fa-plus"></i><strong>Tambah Perangkat</strong></h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('Perangkat/editp')?>" enctype="multipart/form-data">

                                    <input type="hidden" name="perangkat" value="<?php echo $perangkat['device_code']; ?>">

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Nama Site *</label>
                                        <div class="col-lg-5">
                                            <input type="text" name="site_name" value="<?php echo $perangkat['site_name'];?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Latitude *</label>
                                        <div class="col-lg-5">
                                            <input type="number" value="<?php echo $perangkat['latitude'];?>" min="-1000" step="0.0001" name="latitude" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Longitude *</label>
                                        <div class="col-lg-5">
                                            <input type="number" value="<?php echo $perangkat['longitude'];?>" min="-1000" step="0.0001" name="longitude" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Alamat</label>
                                        <div class="col-lg-5">
                                            <textarea name="address" value="<?php echo $perangkat['address'];?>" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kecamatan</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['area'];?>" name="area" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Provinsi</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['province'];?>" name="province" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Zona Waktu</label>
                                        <div class="col-lg-5">
                                            <select class="form-control" class="time_zone" name="time_zone">
                                                <option>Pilih Zona Waktu</option>
                                                <option <?php if($perangkat['time_zone'] == 'wib') echo "selected";?> value="wib">WIB</option>
                                                <option <?php if($perangkat['time_zone'] == 'wit') echo "selected";?> value="wit">WIT</option>
                                                <option <?php if($perangkat['time_zone'] == 'wita') echo "selected";?> value="wita">WITA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kapasitas Terpasang (kWp)</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['capacity'];?>" name="capacity" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Nomor Mobile Site</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['no_mobile_site'];?>" name="no_mobile_site" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kontak Operator</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['contact_operator'];?>" name="contact_operator" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">No Telepon HP</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['phone_number'];?>" name="phone_number" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Kontraktor Pemasang</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['install_contractor'];?>" name="install_contractor" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Tahun Pasang</label>
                                        <div class="col-lg-5">
                                            <input type="text" value="<?php echo $perangkat['install_year'];?>" name="install_year" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">File Manual Book</label>
                                        <div class="col-lg-5">
                                            <input type="file" value="<?php echo $perangkat['manual_book'];?>" id="manual_book" name="manual_book" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Wiring Diagram</label>
                                        <div class="col-lg-5">
                                            <input type="file" value="<?php echo $perangkat['wiring_diagram'];?>" id="wiring_diagram" name="wiring_diagram" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="plts" class="col-lg-3 control-label">Dokumen Survey</label>
                                        <div class="col-lg-5">
                                            <input type="file" value="<?php echo $perangkat['survey_document'];?>" id="survey_document" name="survey_document" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-5">
                                            <a href="<?php echo base_url("Dashboard")?>" class="btn btn-default">Kembali</a>
                                            <button type="submit" class="btn btn-success">Ubah</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</section>