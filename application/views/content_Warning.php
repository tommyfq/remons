<!--main content start-->
<section id="main-content">
  <section class="wrapper">
		
  	<div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-info-circle"></i> Warning Monitoring</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-info-circle"></i>Warning Monitoring</li>
        </ol>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2><i class="fa fa-globe"></i><strong>Pesan Peringatan Remote Monitoring <?php echo date('Y-m-d H:i:s')?></strong></h2>
          </div>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="text-align:center" width="20%">Kode Site</th>
                <th style="text-align:center" width="20%">Waktu</th>
                <th width="60%">Deskripsi</th>
              </tr>
            </thead>

            <tbody>
              <!--Loop Data Warning-->
              <?php foreach($data_warning as $data){ $datediff = (time() - strtotime($data['date']." ".$data['time'])) / 3600 ?>
              <tr>
                <td class="td-data-warning"><b><?php echo (!empty($data['site_name'])) ? $data['device_code']." (".$data['site_name'].")" : $data['device_code']; ?></b></td>
                <!--If Data not empty-->
                <?php if(!empty($data['date'])){ ?>
                  <td class="td-data-warning"><?php echo $data['date']." ".$data['time']; ?></td>
                  <!--Data not updated for 2 days-->
                  <?php if(($datediff / 24) > 2){ ?>
                  <td>
                    <div style="margin:auto" class="alert alert-danger">
                      <?php echo "Terjadi masalah pada perangkat selama ".floor($datediff)." hari";?>
                    </div>
                  </td>
                  <!--Data not updated for 1 days-->
                  <?php }elseif(($datediff) >= 2){ ?>
                  <td>
                    <div style="margin:auto" class="alert alert-warning data-warning">
                      <?php echo "Terjadi masalah pada perangkat selama ".floor($datediff)." jam";?>
                    </div>
                  </td>
                  <!--Data updated-->
                  <?php }else{ ?>
                    <td>
                    <div style="margin:auto" class="alert alert-success">
                      <?php echo "Tidak terjadi masalah pada perangkat"?>
                    </div>
                  </td>
                  <?php } ?>
                <!--If Data empty-->
                <?php }else{ ?>
                  <td class="td-data-warning"></td>
                  <td>Tidak ada data</td>
                <?php } ?>                
              </tr>
              <?php } ?>
              <!--End of loop-->
            </tbody>
          </table>
        </div>  
      </div>
    </div>

	</section>
</section>