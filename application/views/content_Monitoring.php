<!--main content start-->

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Monitoring</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-laptop"></i>Monitoring</li>
                </ol>
            </div>
        </div>
        <?php setlocale(LC_ALL, 'IND'); if($this->session->flashdata('new_device_success') != null) { ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('new_device_success'); ?>
        </div>
        
        <?php } ?>
        <?php if($hour_diff >= 24 && $hour_diff < 48){ ?>
            <div class="alert alert-warning">
                Terjadi masalah pada data perangkat <b><?php echo ($this->input->get('device') == null) ? $devices[0]['device_code'] : $this->input->get('device'); ?></b>. Data terakhir yang dihasilkan <b><?php echo $last_data['date']; ?></b>
            </div>
        <?php } ?>
        <?php if($hour_diff >=  48){ ?>
            <div class="alert alert-danger">
                Terjadi masalah pada data perangkat <b><?php echo ($this->input->get('device') == null) ? $devices[0]['device_code'] : $this->input->get('device'); ?></b>. Data terakhir yang dihasilkan <b><?php echo $last_data['date']; ?></b>
            </div>
        <?php } ?>
        <?php if(!empty($devices)) { ?>
        <div class="row">                                      
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><i class="fa fa-file-text-o"></i><strong>Data Monitoring</strong></h2>
                    </div>
                    <div class="panel-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!--tab nav start-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <section class="panel">
                                            <header class="panel-heading no-border">
                                                Data monitor pada tanggal <b><?php echo $last_data['date']." ".$last_data['time']; ?></b>
                                            </header>
                                            <br>
                                            <table class="table">
                                                <tr>
                                                    <td width="10%">Energi Beban</td>
                                                    <td width="50%"><?php echo $last_data['energi_total']; ?> kWh</td>
                                                </tr>
                                                <tr>
                                                    <td>Frekuensi</td>
                                                    <td><?php echo $last_data['frekuensi']; ?> Hz</td>
                                                </tr>
                                                <tr>
                                                    <td>Daya Total</td>
                                                    <td><?php echo $last_data['daya_total']; ?> kW</td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="30%"></th>
                                                        <th style="text-align:center;">L1</th>
                                                        <th style="text-align:center;">L2</th>
                                                        <th style="text-align:center;">L3</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Daya (kW) </td>
                                                        <td>
                                                            <?php echo $last_data['daya_l1']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['daya_l2']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['daya_l3']; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Arus (A)</td>
                                                        <td>
                                                            <?php echo $last_data['arus_l1']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['arus_l2']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['arus_l3']; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tegangan (V)</td>
                                                        <td>
                                                            <?php echo $last_data['tegangan_l1']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['tegangan_l2']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $last_data['tegangan_l3']; ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </section>
                                    </div>

                                    <!--<div class="col-sm-4">
                                        <section class="panel">
                                            <header class="panel-heading no-border">
                                            </header>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Tipe Pesan</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Error</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Warning</td>
                                                        <td>0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unknown</td>
                                                        <td>0</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </section>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><i class="fa fa-globe"></i><strong>Pilih Data Monitoring</strong></h2>
                    </div>
                    <!-- Panel Body -->
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="get" action="Monitoring">
                                    <div class="form-group">
                                        <label for="plts" class="col-lg-2 control-label">Perangkat</label>
                                        <div class="col-lg-10">
                                            <select class="form-control m-bot15" name="device">
                                                <?php 
													foreach($devices as $device){
                                                        if($device['device_code'] == $this->input->get('device'))
                                                        echo "<option value=".$device['device_code']." selected>".$device["device_code"]." - ".$device['site_name']."</option>";
                                                        else echo "<option value=".$device['device_code']." >".$device["device_code"]." - ".$device['site_name']."</option>";
													} 
												?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal" class="col-lg-2 control-label">Pilih Tanggal</label>
                                        <div class="col-lg-10">
                                            <input type="date" name="date" class="form-control" value="<?php echo $date; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-5">
                                            <button type="submit" class="btn btn-success">Tampilkan Report dan
                                                Grafik</button>
                                        </div>
                                        <div class="col-lg-5" align="right">
                                            <button type="button" onclick="location.href='Tambah_Perangkat_User';"
                                                class="btn btn-primary">Tambah Perangkat</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                        <hr>

                        <div class="row" style='padding:0 50px 0 50px'>
                            <div class="col-12 text-center">
                                <h4><b>Data Daya <?php echo date("d-m-Y", strtotime($date)); ?> (kWh)</b></h4>
                                <canvas id="canvas_hour_power" width="4" height="1"></canvas>
                            </div>
                        </div>
                        <div class="row" style='padding:0 50px 0 50px'>
                            <div class="col-12 text-center">
                                <h4><b>Data Energi <?php echo date("d-m-Y", strtotime($date)); ?> (kWh)</b></h4>
                                <canvas id="canvas_hour_energy" width="4" height="1"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 text-center">
                                <h4><b>Data Energi Bulan <?php echo date("F", strtotime($date)); ?> (kWh)</b></h4>
                                <canvas id="canvas_day_energy" width="3" height="1"></canvas>
                            </div>
                            <div class="col-sm-12 col-md-6 text-center">
                                <h4><b>Data Energi Tahun <?php echo date("Y", strtotime($date)); ?> (kWh)</b></h4>
                                <canvas id="canvas_month_energy" width="3" height="1"></canvas>
                            </div>
                        </div>

                        <div class="row">
                            
                        </div>

                    </div>
                    <!-- End of Panel Body -->
                </div>
            </div>
        </div>
        <?php }else{ ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2><i class="fa fa-plus"></i><strong>Tambah Perangkat User</strong></h2>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal" method="post" action="Tambah_Perangkat_User/addDevice">
                                        <div class="form-group">
                                            <label for="plts" class="col-lg-3 control-label">Kode Lisensi</label>
                                            <div class="col-lg-5">
                                                <input required pattern="([A-z0-9]){16,}" type="text" name="license_code" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="plts" class="col-lg-3 control-label">Kode Perangkat</label>
                                            <div class="col-lg-5">
                                                <input required type="text" name="device_code" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-5">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
</section>