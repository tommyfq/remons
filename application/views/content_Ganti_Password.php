<!--main content start-->
<section id="main-content">
  <section class="wrapper">

    <?php
      $error_msg=$this->session->flashdata('error_msg');
      
      if($error_msg){
      echo $error_msg;
      }
    ?>

  	<div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Ganti Password</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i>Ganti Password</li>
        </ol>
      </div>
    </div>
		
		<div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2><i class="fa fa-plus"></i><strong>Form</strong></h2>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
              	<form class="form-horizontal" id="ganti_pass" method="post" action="Ganti_Password/edit">
	                <div class="form-group">
	                  <label for="plts" class="col-lg-3 control-label">Password Lama</label>
	                  <div class="col-lg-5">
	                    <input type="Password" name="oldpass" class="form-control" required>
	                  </div>
	                </div>

                  <div class="form-group">
                    <label for="plts" class="col-lg-3 control-label">Password Baru</label>
                    <div class="col-lg-5">
                      <input type="Password" name="newpass" id="newpass" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
									title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="plts" class="col-lg-3 control-label">Konfirmasi Password Baru</label>
                    <div class="col-lg-5">
                      <input type="Password" name="newpassconf" id="newpassconf" class="form-control" required>
                    </div>
                    <span class="error col-lg-3" style="color:red"></span>
                  </div>

	                <div class="form-group">
	                  <div class="col-lg-offset-3 col-lg-5">
	                    <button type="submit" class="btn btn-success" id="submit">Submit</button>
	                  </div>
	                </div>

	              </form>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>	

	</section>
</section>

