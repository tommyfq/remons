<!--main content start-->
<section id="main-content">
  <section class="wrapper">
		
  	<div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Monitoring</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-laptop"></i> <a href="Monitoring"> Monitoring</a></li>
          <li>Tambah Perangkat</li>
        </ol>
      </div>
    </div>
	
		<div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2><i class="fa fa-plus"></i><strong>Tambah Perangkat User</strong></h2>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <?php if($this->session->flashdata('new_device_error') != null) { ?>
                <div class="alert alert-danger">
                  <?php echo $this->session->flashdata('new_device_error'); ?>
                </div>
                <?php } ?>
              	<form class="form-horizontal" method="post" action="Tambah_Perangkat_User/addDevice">

	                <div class="form-group">
                    <label for="plts" class="col-lg-3 control-label">Kode Lisensi</label>
                    <div class="col-lg-5">
                      <input required pattern="([A-z0-9]){16,}" maxlength="16" type="text" name="license_code" class="form-control" title="Nomor kode lisensi harus 16 digit">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="plts" class="col-lg-3 control-label">Kode Perangkat</label>
                    <div class="col-lg-5">
                      <input required type="text" name="device_code" class="form-control">
                    </div>
                  </div>
	                <div class="form-group">
	                  <div class="col-lg-offset-3 col-lg-5">
	                    <button type="submit" class="btn btn-success">Submit</button>
	                  </div>
	                </div>
	              </form>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>	

	</section>
</section>