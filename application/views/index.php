<!DOCTYPE html>
<html lang="en">
    
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Remons</title>
        <!-- Load Roboto font -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/pluton.css" />
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/Pluton/css/animate.css" />
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/Pluton/images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/Pluton/images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/Pluton/images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/Pluton/images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/remons-icon.png">

        <!--Facebook Meta Properties-->
        <meta property="og:url"           content="http://remonscloud.com/Remons" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Remonscloud" />
        <meta property="og:description"   content="Remons adalah remote monitoring system untuk mempermudah pengecekan serta pendataan kelistrikan dan beberapa sensor lainnya secara Online." />
        <meta property="og:image"         content="<?php echo base_url();?>assets/images/Remons_icon.png" />
    </head>
    
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <a href="#" class="brand">
                        <img src="<?php echo base_url(); ?>assets/images/remons.png" width="120" height="40" alt="Logo" />
                        <!-- This is website logo -->
                    </a>
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation">
                            <li class="active"><a href="#home">Home</a></li>
                            <li><a href="#service">Services</a></li>
                                <!--<li><a href="#about">About</a></li>-->
                            <li><a href="#contact">Contact</a></li>
                            <li><a href="Login">Masuk / Daftar</a></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
                <div class="triangle"></div>
                <!-- mask elemet use for masking background image -->
                <div class="mask"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                        <h2 class="fittext2">Display Remons</h2>
                        <h4>Layar Sentuh Multi Informasi Berbahasa Indonesia</h4>
                        <p>Display Remons didesain dengan menggunakan layar sentuh yang dapat menampilkan berbagai informasi mengenai PLTS dengan menggunakan bahasa Indonesia.</p>
                        <!-- <a href="#" class="da-link button">Read more</a> -->
                        <div class="da-img">
                            <img src="<?php echo base_url(); ?>assets/Pluton/images/Slider01.png" alt="image01" width="320">
                        </div>
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
                    <div class="da-slide">
                        <h2>Media Penyimpanan</h2>
                        <h4>RAID Teknologi</h4>
                        <p>Redundant Array Of Independent Disk (RAID) memungkinkan penyimpanan data pada komputer dengan fitur toleransi kesalahan di media penyimpanan dengan memakai cara redundansi atau penumpukkan data.</p>
                        <!-- <a href="#" class="da-link button">Read more</a> -->
                        <div class="da-img">
                            <img src="<?php echo base_url(); ?>assets/Pluton/images/Slider02.png" width="320" alt="image02">
                        </div>
                    </div>
                    <!-- End second slide -->
                    <!-- Start third slide -->
                    <div class="da-slide">
                        <h2>Dukungan Remons</h2>
                        <h4>3G, TCP/IP, FTP, CSV Data</h4>
                        <p>Remons menggunakan koneksi internet 3G, dengan mendukung protokol jaringan TCP/IP dan FTP, dapat Memonitor sampai 10 parameter (RS-485) atau sampai 30 parameter (RS-232), dan laporan di-export ke dalam bentuk CSV.</p>
                        <!-- <a href="#" class="da-link button">Read more</a> -->
                        <div class="da-img">
                            <img src="<?php echo base_url(); ?>assets/Pluton/images/Slider03.png" width="320" alt="image03">
                        </div>
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>What We Do?</h1>
                    <!-- Section's title goes here -->
                    <p>Kami membuat remote monitoring system untuk mempermudah pengecekan serta pendataan kelistrikan dan beberapa sensor lainnya secara Online.</p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="<?php echo base_url(); ?>assets/Pluton/images/Service1.png" alt="service 1">
                            </div>
                            <h3>Realtime Monitoring</h3>
                            <p>Mengukur performa site secara realtime dan mengurangi keterlambatan pengumpulan data.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="<?php echo base_url(); ?>assets/Pluton/images/Service2.png" alt="service 2" />
                            </div>
                            <h3>Digitalization</h3>
                            <p>Kemudahan dari digitalisasi untuk tindak-lanjut analisa dalam upaya peningkatan kualitas layanan.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="<?php echo base_url(); ?>assets/Pluton/images/Service3.png" alt="service 3">
                            </div>
                            <h3>Cost Effective</h3>
                            <p>Menghemat sumber daya dan biaya pemantauan Anda</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service section end -->
      
        <!-- Contact section start -->
        <div id="contact" class="contact">
            <div class="section secondary-section">
                <div class="container">
                    <div class="title">
                        <h1>Contact Us</h1>
                        <p>For More Information You Can Contact Us Belows</p>
                    </div>
                </div>
                <div class="map-wrapper">
                    <div class="map-canvas" id="map-canvas">Loading map...</div>
                    <div class="container">
                        <div class="row-fluid">
                            <div class="span5 contact-form centered">
                                <h3>Say Hello</h3>
                                <div id="successSend" class="alert alert-success invisible">
                                    <strong>Well done!</strong>Your message has been sent.</div>
                                <div id="errorSend" class="alert alert-error invisible">There was an error.</div>
                                <form id="contact-form" action="<?php echo base_url(); ?>assets/Pluton/php/mail.php">
                                    <div class="control-group">
                                        <div class="controls">
                                            <input class="span12" type="text" id="name" name="name" placeholder="* Your name..." />
                                            <div class="error left-align" id="err-name">Please enter name.</div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <input class="span12" type="email" name="email" id="email" placeholder="* Your email..." />
                                            <div class="error left-align" id="err-email">Please enter valid email adress.</div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <textarea class="span12" name="comment" id="comment" placeholder="* Comments..."></textarea>
                                            <div class="error left-align" id="err-comment">Please enter your comment.</div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <button id="send-mail" class="message-btn">Send message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="span9 center contact-info">
                            <p>Komplek Green Lake City , Rukan Wallstreet Nomor B-71  </br> Duri Kosambi, Cengkareng.  </br> 
                            http://www.duoms.id/</p>
                        
                        
                        <p class="info-mail">info@duoms.id</p>
                        <p>(+62)21 5433 6081</p>
                        <p>(+62)811 8754 586</p>
                       <div class="title">
                            <h3>Our Social Media</h3>
                        </div>
                    </div>
                    <div class="row-fluid centered">
                        <ul class="social">
                            <li>
                                <a href="">
                                    <span class="icon-facebook-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-twitter-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-linkedin-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-pinterest-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-dribbble-circled"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-gplus-circled"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section edn -->
        <!-- Footer section start -->
        <div class="footer">
            <p>&copy; 2013 Theme by <a href="#">GraphBerry</a>, <a href="#">Documentation</a></p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>
        <!-- ScrollUp button end -->
        <!-- Include javascript -->
        <script src="<?php echo base_url(); ?>assets/Pluton/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/jquery.cslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/Pluton/js/app.js"></script>
    </body>
</html>