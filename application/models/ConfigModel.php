<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigModel extends CI_Model {
    public function getConfig($type,$function){

        $this->db->select('attribute, value');
        $this->db->from('config');
        $this->db->where('type', $type);
        $this->db->where('function', $function);
        $this->db->or_where('type', $type);
        $this->db->where('function', 'general');
        
        return $this->db->get()->result_array();
    }

}