<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DeviceModel extends CI_Model {

    public function assignDevice($data){
        $this->db->where('license_code',$data['license_code']);
        $this->db->update('device',$data);
    }

    public function insertNewDevice($data){
        $this->db->insert('device',$data);
    }

    public function updateDevice($data){
        $this->db->where('device_code', $data['device_code']);
        $this->db->update('device', $data);
    }

    public function checkAssignedDevice_BAK($license_code, $device_code, $user_id){
        //var_dump($device_code);
        //die;
        $isValid = true;

        $this->db->select("id");
        $this->db->where('license_code',$license_code);
        $res = $this->db->get('device')->row_array();
        
        if($res == null) $isValid = false;

        $this->db->select("id");
        $this->db->where('device_code', $device_code);
        $res = $this->db->get('device')->row_array();

        if($res == null) $isValid = false;
        //var_dump($device_code);
        //die;
        $this->db->select('id, device_code');
        $this->db->where(array('device_code =' => $device_code, 'user_id =' => $user_id));
        $this->db->or_where('license_code',$license_code);
        $this->db->where('user_id',$user_id);
        $res = $this->db->get('device')->row_array();
        $str = $this->db->last_query();
        
        if($res != null) $isValid = false;
        //var_dump($res);
        //die;
        return $isValid; 

    }

    public function checkAssignedDevice($license_code, $device_code, $user_id){
        $this->db->select('status, user_id');
        $this->db->where('license_code', $license_code);
        $this->db->where('device_code', $device_code);
        $res = $this->db->get('device')->row();
        
        if ($res->status == '0' || $res->status == NULL && $res->user_id == NULL) {
            return true;
        }else{
            return false;
        }
        
    }

    public function getUserDevice($user_id){
        $this->db->select("*");
        $this->db->from('device');
        $this->db->where('user_id',$user_id);
        $res = $this->db->get();
        
        return $res->result_array();
    }

    public function getActiveDevice(){
        
        $this->db->select('d.device_code');
        $this->db->from('device as d');
        $this->db->where('d.user_id !=', null);
        $this->db->where('d.status', '1');
        
        $res = $this->db->get()->result_array();
        
        return $res;
    }

    public function getRowDevice($device_code, $user_id){
        $this->db->select('*');
        $this->db->where('device_code', $device_code);
        $this->db->where('user_id', $user_id);
        return $this->db->get('device')->row_array();
    }

}