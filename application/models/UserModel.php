<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function checkUser($username, $password){
    $query = $this->db->query("SELECT * FROM tbl_user WHERE username = '$username' AND password = '$password'");
    return $query->num_rows();
  }

  public function get_user($username){
    $this->db->select('tu.*, tud.status, tud.token');
    $this->db->from('tbl_user tu');
    $this->db->join('tbl_user_detail tud', 'tud.userid = tu.id', 'left');
    $this->db->where('tu.username',$username);
    $query = $this->db->get();

    return $query->row_array();
  }

  public function getUserById($userid)
  {
    $this->db->select('tu.*, tud.*');
    $this->db->from('tbl_user tu');
    $this->db->join('tbl_user_detail tud', 'tud.userid = tu.id', 'left');
    $this->db->where('tu.id', $userid);
    $query = $this->db->get()->row();

    return $query;
  }

  public function Register($user_register){
   $this->db->insert('tbl_user', $user_register);
   return $this->db->insert_id();
  }

  public function email_check($email){
    $this->db->select('*');
    $this->db->from('tbl_user');
    $this->db->where('email',$email);
    $query=$this->db->get();

    if($query->num_rows()>0){
      return false;
    }else{
      return true;
    }
  }

  public function username_check($username){
    $this->db->select('tu.*');
    $this->db->from('tbl_user tu'); 
    $this->db->where('tu.username',$username);
    $query=$this->db->get();
    
    if($query->num_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  public function check_subscribe($username){
    $this->db->select('tbl_user.id_subs, tbl_subs.jenis');
    $this->db->from('tbl_user');
    $this->db->join('tbl_subs', 'tbl_subs.id = tbl_user.id_subs');
    $this->db->where('tbl_user.username', $username);

    $query = $this->db->get();
    return $query->row_array();
  }

  public function getUserCredential($username){
    $this->db->select('password');
    $this->db->from('tbl_user');
    $this->db->where('username', $username);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
      return $query->row()->password; 
    }else{
      return false;
    }
  }

  public function addUserDetail($userid){
    $this->db->select('userid');
    $this->db->where('userid', $userid);
    $check = $this->db->get('tbl_user_detail')->row();
    
    if ($check == NULL) {
      $data = array(
        'userid'  => $userid,
        'token'   => $this->getToken(),
        'tokendt' => date("Y-m-d H:i:s"),
        'status'  => 0 
      );
      $this->db->insert('tbl_user_detail', $data);
  
      return true;
    }else{
      return false;
    }
  }

  public function updateUserDetail($userid){
    $this->db->select('userid');
    $this->db->where('userid', $userid);
    $check = $this->db->get('tbl_user_detail')->row()->userid;
    
    if ($check != NULL) {
      $data = array(
        'token'   => $this->getToken(),
        'tokendt' => date("Y-m-d H:i:s")
      );

      $this->db->where('userid', $userid);
      $this->db->update('tbl_user_detail', $data);
  
      return true;
    }else{
      return false;
    }
  }
  
  public function updateStatus($userid)
  {
    $this->db->set('status', '1');
    $this->db->set('activedt', date("Y-m-d H:i:s"));
    $this->db->where('userid', $userid);
    $this->db->update('tbl_user_detail');
  }

  public function getToken(){
    $token = "";
    $length = 30;
    $randAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $randAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $randAlphabet.= "0123456789";
    $max = strlen($randAlphabet);

    for ($i=0; $i < $length; $i++) {
      $rand = $this->random_token(0, $max-1);
      
      $token .= $randAlphabet[$rand];
    }

    return $token;
  }

  function random_token($min, $max){
    $range = $max - $min;
    if ($range < 1) return $min;
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1;
    $bits = (int) $log + 1;
    $filter = (int) (1 << $bits) - 1; 
    do {
      $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
      $rnd = $rnd & $filter; 
    } while ($rnd > $range);
    
    return $min + $rnd;
  }

}