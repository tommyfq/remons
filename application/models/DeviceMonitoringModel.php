<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DeviceMonitoringModel extends CI_Model {

    public function insertDeviceData($device_code, $data_monitoring, $data_monitoring_akumulasi){
        $result = true;
        try{
            //Insert Monitoring Data
            $res = $this->db->insert_batch('device_monitoring_data', $data_monitoring);
            
            //Update Monitoring Data
            $this->db->select('device_code');
            $this->db->where('device_code', $device_code);
            $dmda = $this->db->get('device_monitoring_data_akumulasi')->row_array();
            if($dmda != null){
                $this->db->where('device_code', $device_code);
                $this->db->update('device_monitoring_data_akumulasi', $data_monitoring_akumulasi);
            }else{
                $this->db->insert('device_monitoring_data_akumulasi', $data_monitoring_akumulasi);
            }
            
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public function getLastData($device_code){
        $sql = "SELECT dmad.date, dmad.time, dmad.daya_l1, dmad.daya_l2, dmad.daya_l3, dmad.arus_l1, dmad.arus_l2, dmad.arus_l3, dmad.tegangan_l1, dmad.tegangan_l2, dmad.tegangan_l3, dmad.frekuensi, dmd.energi_total, dmd.daya_total
        FROM device_monitoring_data_akumulasi AS dmad
        LEFT JOIN device_monitoring_data AS dmd ON dmad.device_code = dmd.device_code
        WHERE dmad.device_code = ?
        ORDER BY dmad.id DESC, dmd.id DESC";
        return $this->db->query($sql,array($device_code))->row_array();
    }

    public function getLastPowerData($device_code){
        $this->db->select('daya_total');
        $this->db->where('device_code', $device_code);
        $this->db->order_by('id', 'desc');
        return $this->db->get('device_monitoring_data')->row_array();
    }

    public function getDataDaily($date){
        $this->db->select('energi_total, daya_total, time');
        $this->db->where('date', $date);
        $this->db->order_by('time', 'asc');
        $res = $this->db->get('device_monitoring_data')->result_array();
        return $res;
    }

    public function getDataMonthly($month,$year,$device_code){
        $sql = "SELECT dma1.energi_total, DAY(dma1.date) as day 
        FROM device_monitoring_data AS dma1 
        LEFT JOIN device_monitoring_data AS dma2 ON (MONTH(dma1.date) = MONTH(dma2.date) AND DAY(dma1.date) = DAY(dma2.date) AND TIME(dma1.time) < TIME(dma2.time))
        WHERE dma2.id IS NULL AND MONTH(dma1.date) = ? AND YEAR(dma1.date) = ? AND dma1.device_code = ?";
        
        return $this->db->query($sql,array($month,$year,$device_code))->result_array();
        /*$res = $this->db->query($sql,array($month,$year,$device_code))->result_array();
        highlight_string("<?php\n\$data =\n" . var_export($res, true) . ";\n?>");
        die;*/
    }

    public function getPreviousDataMonthly($date, $device_code){
        $sql = "SELECT energi_total
        FROM device_monitoring_data
        WHERE MONTH(date) < MONTH(?)
        AND device_code = ?
        ORDER BY id DESC
        LIMIT 1";
        /*$res = $this->db->query($sql,array($date))->result_array();
        highlight_string("<?php\n\$data =\n" . var_export($res, true) . ";\n?>");
        die;*/
        return $this->db->query($sql,array($date,$device_code))->row_array();
    }

    public function getDataYearly($year, $device_code){
        $sql = "SELECT dma1.energi_total, MONTH(dma1.date) as month
        FROM device_monitoring_data AS dma1 
        LEFT JOIN device_monitoring_data AS dma2 ON (MONTH(dma1.date) = MONTH(dma2.date) AND DAY(dma1.date) = DAY(dma2.date) AND TIME(dma1.time) < TIME(dma2.time))
        WHERE dma2.id IS NULL AND YEAR(dma1.date) = ? AND dma1.device_code = ?";
        
        return $this->db->query($sql,array($year,$device_code))->result_array();
        /*$res = $this->db->query($sql,array($month))->result_array();
        highlight_string("<?php\n\$data =\n" . var_export($res, true) . ";\n?>");
        die;*/
    }

    public function getPreviousDataYearly($date, $device_code){
        $sql = "SELECT energi_total
        FROM device_monitoring_data
        WHERE YEAR(date) < YEAR(?)
        AND device_code = ?
        ORDER BY id DESC
        LIMIT 1";
        /*$res = $this->db->query($sql,array($date))->result_array();
        highlight_string("<?php\n\$data =\n" . var_export($res, true) . ";\n?>");
        die;*/
        return $this->db->query($sql,array($date,$device_code))->row_array();
    }

    public function getLatestData($userId){
        $this->db->select('*');
        $this->db->join('device_monitoring_data_akumulasi as dmda', 'dmda.device_code = d.device_code', 'left');
        $this->db->where('d.user_id', $userId);
        $data = $this->db->get('device as d')->result_array();

        return $data;
    }

    public function getWarningDeviceUser($userId){
        $this->db->select('d.device_code, dmda.date, dmda.time, d.site_name');
        $this->db->join('device_monitoring_data_akumulasi as dmda', 'dmda.device_code = d.device_code', 'left');
        $this->db->where('d.user_id', $userId);
        $data = $this->db->get('device as d')->result_array();
        
        return $data;
        
    }

}